package ictgradschool.industry.collections.ex02;

import java.util.*;

public class ArrayOfString {

    public static void main(String[] args) {

        String[] array = {"ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN"};

        //CONVERT the array of strings to an ArrayListex1
        List<String> mylist; //empty arrayList
        mylist = Arrays.asList(array);

//        for (int i = 0; i < array.length; i++) { //need to assign values from array[] to new mylist
//            mylist.add(array[i]);
//            System.out.println(mylist.add(array[i]));
//        }

        //Using a loop over the index values
        //1.for loop
        for (int i = 0; i < mylist.size(); i++) {
            mylist.set(i, mylist.get(i).toLowerCase()); //'set' is replacing the list, so if you use 'add' it doesn't work
            mylist.set(i, mylist.get(i).toLowerCase());
            System.out.println(mylist.set(i, mylist.get(i).toLowerCase()));
        }

        int i = 0;
        //2.Using an enhanced for loop
        for (String c : mylist) {  //String type, variable name c, looping through mylist.
            mylist.set(i, c.toLowerCase());
            i++;
        }

        System.out.println(mylist);

        //3.Using an iterator
        ListIterator<String> myiterator = mylist.listIterator(); //getting a new iterator from myList

        i=0;
        while (myiterator.hasNext()) { //if this is true
            String iteratedList = myiterator.next(); //gets the next element in line
            mylist.set(i, iteratedList.toLowerCase());
            //System.out.println(iteratedList.toLowerCase());
            i++;
        }

        System.out.println(mylist);
    }
}








